// Setup dependencies

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Allows all resources to access our backend application
app.use(cors());
app.use(express());
app.use(express.urlencoded({extended: true}));

// db connection
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.vvd8gml.mongodb.net/bookingAPI?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."));

// This syntax will allow flexibility when using the application both locally or as a hosted app
// process.env.PORT can be assigned by your hosting service
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});

// Acitivity

// Create a User.js file to store the schema of our users
// models > User.js